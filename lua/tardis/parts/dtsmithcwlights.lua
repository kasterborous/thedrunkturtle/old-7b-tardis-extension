local PART={}
PART.ID = "dtsmithcwlights"
PART.Name = "dtsmithcwlights"
PART.Model = "models/doctorwho1200/toyota/catwalklightssmith.mdl"
PART.AutoSetup = true
PART.Collision = true

if CLIENT then
 function PART:Think()
  local exterior = self.exterior
  local power = self.exterior:GetData("power-state")
  mat = Material("models/doctorwho1200/toyota/catwalklightsstatic")

   if power == true then
    if exterior:GetData("health-warning") then
      self:SetColor(Color(255,0,0))
    else
      self:SetColor(Color(255,255,255))
    end
    if exterior:GetData("flight") or exterior:GetData("teleport") or exterior:GetData("vortex") then
     self:SetMaterial("models/doctorwho1200/toyota/catwalklights")
    else
     self:SetMaterial("models/doctorwho1200/toyota/catwalklightsstatic")
    end
   else
     self:SetColor(Color(255,255,255))
     self:SetMaterial("models/doctorwho1200/toyota/catwalklightsoff")
   end
 end
end


TARDIS:AddPart(PART,e)