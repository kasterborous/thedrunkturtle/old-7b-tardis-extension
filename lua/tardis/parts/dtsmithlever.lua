local PART={}
PART.ID = "dtsmithlever"
PART.Name = "dtsmithlever"
PART.Model = "models/doctorwho1200/toyota/lever.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 3
PART.Sound = "doctorwho1200/toyota/lever.wav"

if SERVER then
	function PART:Use(activator)
		self:EmitSound(Sound("doctorwho1200/toyota/lever.wav"))
		self.exterior:ToggleFlight()
	end
end

TARDIS:AddPart(PART,e)