local PART={}
PART.ID = "dtsmithflippers"
PART.Name = "dtsmithflippers"
PART.Model = "models/doctorwho1200/toyota/flippers.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 1
if SERVER then
	function PART:Draw()
		self:DrawModel()
	end
	function PART:Use(activator)
		self.exterior:ToggleFloat()
		self:EmitSound( Sound( "doctorwho1200/toyota/flippers.wav" ))
	end
end

TARDIS:AddPart(PART)