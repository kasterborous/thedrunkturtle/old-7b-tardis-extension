local PART={}
PART.ID = "dtlightssmith1"
PART.Name = "lightssmith1"
PART.Model = "models/doctorwho1200/toyota/lights1.mdl"
PART.AutoSetup = true

if CLIENT then
 function PART:Think()
  local exterior = self.exterior
  local power = self.exterior:GetData("power-state")
  mat = Material("models/doctorwho1200/toyota/neonlights")
   if power == true then
    if exterior:GetData("flight") or exterior:GetData("teleport") or exterior:GetData("vortex") then
     self:SetMaterial("")
    else
     self:SetMaterial("models/doctorwho1200/toyota/neonlights")
    end
   else
     self:SetMaterial("models/doctorwho1200/toyota/neonlightsoff")
   end
 end
end

TARDIS:AddPart(PART,e)