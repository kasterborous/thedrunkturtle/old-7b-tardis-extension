local PART={}
PART.ID = "dtsmithintdoors"
PART.Name = "2013 TARDIS Interior Doors"
PART.Model = "models/doctorwho1200/toyota/intdoors.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 1

if SERVER then
	function PART:DontCollide()
		self:SetCollisionGroup(COLLISION_GROUP_WORLD)
	end

	function PART:Collide()
		self:SetCollisionGroup(COLLISION_GROUP_NONE)
	end

	function PART:Use()
		sound.Play("doctorwho1200/toyota/intdoors.wav", self:LocalToWorld(Vector(285,0,0)))
		if ( self:GetOn() ) then
			self:Collide( true )
		else
			self:DontCollide( true )
		end
	end

	function PART:Toggle( bEnable, ply )
		if ( bEnable ) then
			self:SetOn( true )
			self:DontCollide( true )
			sound.Play("doctorwho1200/toyota/intdoors.wav", self:LocalToWorld(Vector(285,0,0)))
		else
			self:SetOn( false )
			self:Collide( true )
			sound.Play("doctorwho1200/toyota/intdoors.wav", self:LocalToWorld(Vector(285,0,0)))
		end
	end
end

TARDIS:AddPart(PART,e)