local PART={}
PART.ID = "dtsmithtelepathic"
PART.Name = "dtsmithtelepathic"
PART.Model = "models/doctorwho1200/toyota/telepathic.mdl"
PART.AutoSetup = true
PART.Collision = true
if SERVER then
	function PART:Use(ply)
        local power = self.exterior:GetData("power-state")
        if power == nil then return end
		TARDIS:Control("destination", ply)
		self:EmitSound(Sound("doctorwho1200/toyota/telepathic.wav"))
	end
end

TARDIS:AddPart(PART,e)