local PART={}
PART.ID = "dtsmithkey"
PART.Name = "dtsmithkey"
PART.Model = "models/doctorwho1200/toyota/key.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 4

if SERVER then
	function PART:Use()
		self.interior:TogglePower()
		if ( self:GetOn() ) then
			self:EmitSound( Sound( "doctorwho1200/toyota/buttons.wav" ))
		else
			self:EmitSound( Sound( "doctorwho1200/toyota/buttons.wav" ))
		end
	end

	function PART:Think()
		local power=self.exterior:GetData("power-state")
		local interior=self.interior
		if power == nil then
			interior:SetSubMaterial(20, "models/doctorwho1200/toyota/black")
		else
			interior:SetSubMaterial(20 , "models/doctorwho1200/toyota/screen")
		end
	end
end

TARDIS:AddPart(PART,e)